#!/usr/bin/env python3
#
# This program outputs names of current doctors at ZD Ljubljana.

import pyquery
import requests

s = requests.Session()

r = s.get('https://www.zd-lj.si/zdlj/index.php?option=com_content&view=article&id=954&Itemid=2870&lang=sl')
r.raise_for_status()
for doctor in pyquery.PyQuery(r.text)("""h2:contains("ZDRAVSTVENO VARSTVO PREDŠOLSKIH OTROK") + table""").find('tr > td:eq(1)'):
  print(doctor.text)
