#!/bin/bash

touch doctors.txt || exit 10

# We have to fetch latest job ID ourselves, because we want
# to fetch the artifact of the latest failed job as well.
# See: https://gitlab.com/gitlab-org/gitlab/-/issues/368626
last_job_id=`curl --fail --location --header "PRIVATE-TOKEN: $API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs?scope[]=failed&scope[]=success" |  jq '.[0].id'`
curl --fail --location --output doctors.txt --header "PRIVATE-TOKEN: $API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$last_job_id/artifacts/doctors.txt?job=run" || exit 10

./zdlj.py > doctors-new.txt || exit 10
diff doctors.txt doctors-new.txt
code="$?"
mv doctors-new.txt doctors.txt || exit 10
exit "$code"
